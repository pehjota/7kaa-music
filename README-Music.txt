Seven Kingdoms: Ancient Adversaries Music Soundtrack
----------------------------------------------------

Provided is the original music from the 1997 release for Seven Kingdoms. The
music soundtrack is not GPL. Please see COPYING-Music.txt for the copyright
information.

To install the music where Seven Kingdoms may properly read the files, place
the MUSIC directory inside the top level game data directory for the
Seven Kingdoms game. Usually this is /usr/share/7kaa or /usr/local/share/7kaa
on Linux, and \Program Files\7kaa on Windows.

PACKAGERS PLEASE READ THE FOLLOWING

See the PACKAGE_DATA_DIR preprocessor definition you are setting when building
the game. This is where the music will need to be when all is done. If you
create a music package separate from the main game, this is where you need to
have your package install to.

Please note that this distribution is intended with version 2.15.1+ of the
game. The directory and files are in uppercase for filesystem consistency.

After packaging is done, you can test the installation by starting the game.
At the main menu, you will hear the WAR.WAV track, provided your in-game
volume and your system volume is turned up.
