Developing the 7kaa-music Package In Git
========================================

Development Dependencies
------------------------

This package is developed using git-buildpackage and pristine-tar.
This author also prefers to use sbuild with `~/.sbuildrc` containing
`$lintian_opts = ['--display-info', '--pedantic'];`, but any clean environment
that runs lintian is fine for test building.

Git Commit Message Style Guide
------------------------------

The following guidelines for commit messages maintain consistency with previous
`debian/changelog` entries and the use of gbp-dch, lintian, etc.:

  * Limit all lines to 76 columns (72 columns is recommended, e.g. by Git
    itself).  When converted to an indented list in `debian/changelog`,
    the resulting lines will be up to 80 columns wide.  lintian will warn
    (debian-changelog-line-too-long) if any lines are wider than 80 columns.
  * End all sentences, including on line 1, with a full stop (`.`), for
    consistent gbp-dch output.  If additional details follow, consider ending
    line 1 with a colon (`:`) instead.
  * Skip line 2 and begin additional details, if any, on line 3.  Git joins all
    lines until the first empty line, resulting in one long summary line.
  * Begin list items with `+ ` (convention of longtime 7kaa package maintainer
    Bertrand Marc) or `- ` on the first line and `  ` on subsequent lines.
  * When committing changes to fix a previous commit within the same release in
    progress, append a blank line and `Gbp-Dch: Ignore` to the commit message.
    `debian/changelog` should describe only changes relative to the previous
    release.

GitLab Merge Requests
---------------------

If merging using the Web interface, check "Edit commit message".  Append a blank
line and `Gbp-Dch: Ignore` to the merge commit message.  "Merge branch 'foo'
into 'master'" changes don't belong in `debian/changelog`.

Typographical Conventions
-------------------------

In the below command blocks, `$ ` is the first-line shell prompt.  Commands
that are too long to fit on one line are split with a `\` escape, a newline, and
the continuation-line shell prompt `> `.  When typing these commands, skip the
`$ ` and `> ` prompts, and you can enter them on a single line without the '\'.

Workflow: New Upstream Version
------------------------------

Clone the Git repository:

    $ gbp clone --all git@salsa.debian.org:games-team/7kaa-music.git
    $ cd 7kaa-music/

Browse <https://www.7kfans.com/download/> and click to browse the latest game
version page.  Copy the "Music (Not GPL) tar.bz" URL and download it (for
example <https://www.7kfans.com/downloads/7kaa-music-2.15.tar.bz2>) as follows:

    $ wget -P .. https://www.7kfans.com/downloads/7kaa-music-2.15.tar.bz2

Or:

    $ VER=2.15 debian/rules get-orig-source
    $ mv 7kaa-music-*.tar.bz2 ../

And import it:

    $ gbp import-orig --import-msg='New upstream version %(version)s.' \
    > --pristine-tar --no-interactive ../7kaa-music-2.15.tar.bz2

If this version has notable changes and/or fixes bugs, amend the commit message
to note them (remember to skip line 2):

    $ git commit --amend

Update `Standards-Version:` and `debhelper-compat` if necessary and make any
other changes.

Update `debian/changelog` and release:

    $ gbp dch --full --release --new-version="$(git tag --list 'upstream/*' \
    > --sort=-version:refname | sed '1{ s|^upstream/||; s|$|-1|; q; }')" \
    > --git-log='master --not upstream' --git-author --multimaint-merge \
    > --commit-msg='Update changelog for %(version)s release.' --commit

In the spawned editor, manually remove any remaining merge commits (e.g. "See
merge request" commits from GitLab merge requests) **except for a "New upstream
version" commit**.

Build:

    $ gbp buildpackage --git-pristine-tar --git-builder=sbuild

Push to GitLab:

    $ git push --tags origin master:master upstream:upstream \
    > pristine-tar:pristine-tar

After the package is uploaded to the Debian package archive, tag the release:

    $ git tag "debian/$(dpkg-parsechangelog --show-field Version)"
    $ git push --tags origin

 -- P. J. McDermott  Fri, 15 Dec 2023 09:17:38 -0500
